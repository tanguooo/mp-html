/**
 * @fileoverview audio 组件
 */
const context = require('./context')
// 左侧补零
function addZero(val) {
  if (val < 10) {
    return 0 + '' + val
  } else {
    return val
  }
}
let caindex = 0;
// 时间格式化
function formatSeconds(value) {
  let secondTime = parseInt(value); // 秒
  let minuteTime = 0; // 分
  let hourTime = 0; // 小时
  if (secondTime > 60) {
    minuteTime = parseInt(secondTime / 60);
    secondTime = parseInt(secondTime % 60);
    if (minuteTime > 60) {
      hourTime = parseInt(minuteTime / 60);
      minuteTime = parseInt(minuteTime % 60);
    }
  }
  let result = "" + addZero(parseInt(secondTime)) + "";
  result = "" + addZero(parseInt(minuteTime)) + ":" + result;
  if (hourTime > 0) {
    result = "" + parseInt(hourTime) + ":" + result;
  }
  return result;
}

function pauseAllOthers(playid) {
  const ctxs = context.getAll();
  Object.keys(ctxs).forEach(id => {
    if (ctxs[id] && id !== playid && id.startsWith('ca') && typeof ctxs[id].pause === 'function') {
      ctxs[id].pause();
    }
  });
}

Component({
  data: {
    playing: false, // 播放的flag
    duration: '00:00', // 播放时长     时间格式
    current: '00:00', // 当前播放时长   时间格式
    durationNum: 0, // 播放时长数字     数字格式
    currentNum: 0, // 当前播放时长数字   数字格式
    loadDone: false,
    needSeek: false
  },
  properties: {
    autoplay: Boolean, // 是否循环播放
    src: {
      type: String
    } // 音频网址
  },
  ready() {
    this.audio.src = this.data.src;
    if (this.data.autoplay)
      this.audio.autoplay = true;
    this.interval = setInterval(() => {
      const n = Date.now();
      // Print debug info every 10s
      if (!this.data.loadDone || parseInt(n / 1000) % 10 === 0) {
        console.log(Date.now(), 'onTimeUpdate', 'paused:', this.audio.paused, 'playing:', this.data.playing, 'duration:', this.audio.duration, 'current', this.audio.currentTime);
      }
      if (this.data.loadDone && this.data.durationNum === 0 && !isNaN(parseInt(this.audio.duration)) && parseInt(this.audio.duration) !== 0) {
        let durationnum = parseInt(this.audio.duration);
        let time = formatSeconds(this.audio.duration);
        this.setData({
          duration: time,
          durationNum: durationnum,
        });
        console.log(Date.now(), 'onTimeUpdate duration set', 'paused:', this.audio.paused, 'playing:', this.data.playing, 'duration:', this.audio.duration, 'current', this.audio.currentTime);
      }
      if (!this.audio.paused) {
        this.changeCurrent(this.audio.currentTime);
      }
    }, 300);
    wx.showLoading();
    this.audio.onWaiting(() => {
      console.log('onWaiting, duration:', this.audio.duration, 'current:', this.audio.currentTime);
      wx.showLoading();
    });
    this.audio.onPlay((res) => {
      wx.hideLoading({
        fail:()=>{}
      });
      // 必须读取一次audio.duration ，否则onTimeUpdate不工作
      console.log('onPlay, duration:', this.audio.duration, 'current:', this.audio.currentTime);
      pauseAllOthers(this.caid);
      this.setData({
        playing: true
      });
    });
    this.audio.onCanplay(() => {
      wx.hideLoading({
        fail:()=>{}
      });      if (!this.data.loadDone) {
        this.setData({
          loadDone: true
        });
      }
      // 必须读取一次audio.paused ，否则onTimeUpdate不工作
      console.log('onCanplay, duration:', this.audio.duration, 'current:', this.audio.currentTime, 'paused:', this.audio.paused, 'playing:', this.data.playing);
    });
    this.audio.onEnded(() => {
      this.changeCurrent(0);
      console.log('onEnded');
      this.setData({
        playing: false
      });
      this.changeCurrent(0);
      this.seek(this.data.currentNum);
    });
    // this.audio.onTimeUpdate(() => {
    //   console.log(Date.now(), 'onTimeUpdate');
    //   if (!this.data.loadDone) {
    //     let durationnum = parseInt(this.audio.duration);
    //     let time = formatSeconds(this.audio.duration);
    //     //this.audio.pause();
    //     this.setData({
    //       loadDone: true,
    //       duration: time,
    //       durationNum: durationnum,
    //     });
    //     console.log(`load done. duration: ${time}`);
    //   }
    //   this.changeCurrent(this.audio.currentTime)
    // });
    this.audio.onPause(() => {
      console.log('onPause, duration:', this.audio.duration, 'current:', this.audio.currentTime, 'paused:', this.audio.paused);
      this.setData({
        playing: false
      })
    });
  },
  attached() {
    this.audio = wx.createInnerAudioContext();
    this.caid = 'ca-' + caindex.toString();
    caindex++;
    context.set(this.caid, this);
    console.log(Date.now(), 'audio attached', this.caid);
  },
  detached() {
    context.delete(this.caid);
    if (this.interval) {
      clearInterval(this.interval);
      delete this.interval;
    }
    console.log(Date.now(), 'after detach', this.caid, context.getAll());
    if (this.audio)
      this.audio.destroy();
  },
  // #ifndef ALIPAY | TOUTIAO
  pageLifetimes: {
    show() {
      // 播放被后台打断时，页面显示后自动继续播放
      if (this.data.playing && this.audio.paused) {
        this.audio.play()
      }
    }
  },
  // #endif
  methods: {
    // 播放暂停
    play() {
      if (!this.data.playing) {
        // Windows下，停止再播放会出现播放时间跳跃的现象，发现调用audio.play之前先audio.seek往后退一定时间可以避免这个问题;
        // 如果是在go/back/changeValue中已经调用过seek，这里就不再调用了，否则之前的设置会被覆盖
        if (this.needSeek && typeof this.audio.currentTime == 'number' && !isNaN(this.audio.currentTime)) {
          let to = this.audio.currentTime - 1;
          if (to < 0) {
            to = 0;
          }
          console.log('play action seek to:', to);
          this.changeCurrent(to);
          this.audio.seek(this.data.currentNum);
        }
        this.needSeek = true;
        this.audio.play();
      }
    },
    pause() {
      if (this.data.playing) {
        this.audio.pause();
      }
    },
    // 快进
    go() {
      if (this.data.playing) {
        let to = parseInt(this.audio.currentTime) + 10;
        if (to > this.data.durationNum)
          to = 0;
        this.audio.pause();
        this.audio.seek(to);
        this.audio.play();
      } else {
        let to = this.data.currentNum + 10;
        if (to > this.data.durationNum)
          to = 0;
        this.changeCurrent(to);
        this.seek(this.data.currentNum);
      }
    },
    // 快退
    back() {
      if (this.data.playing) {
        let to = parseInt(this.audio.currentTime) - 10;
        if (to < 0)
          to = 0;
        this.audio.pause();
        this.audio.seek(to);
        this.audio.play();
      } else {
        let to = this.data.currentNum - 10;
        if (to < 0)
          to = 0;
        this.changeCurrent(to);
        this.seek(this.data.currentNum);
      }
    },
    // 滑块拖动快进，快退
    changeValue(e) {
      let val = e.detail.value;
      let step = (val / 100) * this.data.durationNum;
      if (this.data.playing) {
        this.audio.pause();
        this.audio.seek(step);
        this.audio.play();
      } else {
        this.changeCurrent(step);
        this.seek(this.data.currentNum);
      }
    },
    // 当前播放格式化
    changeCurrent(step) {
      let currentnum = parseInt(step)
      let currentt = formatSeconds(currentnum)
      this.setData({
        current: currentt,
        currentNum: step
      })
    },
    seek(to) {
      this.needSeek = false;
      this.audio.seek(to);
    }
  },
})
